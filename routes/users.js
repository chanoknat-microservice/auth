const express = require('express');
const router = express.Router();
const argon2 = require('argon2');
const { QueryTypes } = require('sequelize');
const jwt = require('jsonwebtoken');

const model = require('../models/index');
const passportJWT = require('../middlewares/passport-jwt');

/* get current profile 
  /api/v1/users/profile
*/
router.get('/profile', [passportJWT.isLogin] , async function(req, res, next) {
  const user = await model.User.findByPk(req.user.user_id);

  return res.status(200).json({
    id: user.id,
    fullname: user.fullname,
    email: user.email,
    create_at: user.create_at
  });
});


/* /api/v1/users/ */
router.get('/', async function(req, res, next) {
  //res.send('respond with a resource');
  //const users = await model.User.findAll();
  const sql = 'SELECT * FROM `users` ORDER BY id desc';
  const users2 = await model.sequelize.query(sql, { 
  type: QueryTypes.SELECT 
  });

  const users = await model.User.findAll({
    //attributes: ['id', 'fullname']
    attributes:{ exclude: ['password']},
    order: [['id', 'desc']]
  });

  const totalUsers = await model.User.count();

  return res.status(200).json({
    total : totalUsers,
    data : users2
  });
});


/* /api/v1/users/register */
router.post('/register', async function(req, res, next) {
  const {fullname, email, password} = req.body;
  //req.body.fullname... เหมือนกันแต่เขียนแบบ destructuring assignmnent ได้
  
  //1 check duplicate email
  const user = await model.User.findOne({ where: { email: email } });
  if (user !== null) {
    return res.status(400).json({message:'มีผู้ใช้ email นี้แล้ว'});  
  }

  //2 hashing password
  const passwordHash = await argon2.hash(password);

  //3 save in database
  const newUser = await model.User.create({
    fullname: fullname,
    email: email,
    password: passwordHash
  });

  return res.status(201).json({
    user: {
      id: newUser.id,
      fullname: newUser.fullname
    },
    message : "ลงทะเบียนสำเร็จ"
  });
});

/* /api/v1/users/login */
router.post('/login', async function(req, res, next) {
  const { email, password } = req.body;
  //req.body.fullname... เหมือนกันแต่เขียนแบบ destructuring assignmnent ได้
  
  //1 check email in db
  const user = await model.User.findOne({ where: { email: email } });
  if (user === null) {
    return res.status(404).json({message:'ไม่พบผู้ใข้นี้ในระบบ'});  
  }

  //2 verify password
  const isValid = await argon2.verify(user.password, password);
  if(!isValid) {
    return res.status(401).json({message: 'รหัสผ่านไม่ถูกต้อง'});
  }

  //3 create token
  const token = await jwt.sign({ user_id: user.id},process.env.JWT_KEY, { expiresIn: '7d'});
  return res.status(200).json({
    message : "เข้าระบบสำเร็จ",
    access_token: token
  });
});



module.exports = router;
